package com.employee.assignment.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.assignment.dto.EmployeeDto;
import com.employee.assignment.entity.Employee;
import com.employee.assignment.repo.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	EmployeeRepository employeeRepository;

	//Find all employee operation.
	public List<EmployeeDto> findAllEmployee() throws Exception {

		List<EmployeeDto> dtoList = new ArrayList<EmployeeDto>();
		try {
			List<Employee> employee = new ArrayList<Employee>();
			employeeRepository.findAll().forEach(employee::add);
			for (Employee employee2 : employee) {
				dtoList.add(mapDto(employee2));
			}
		} catch (Exception e) {
			throw new Exception("FIND ALL EMPLOYEE EXCEPTION", e);
		}
		return dtoList;
	}

	//Find employee by id operation.
	public EmployeeDto findEmployeeById(String id) throws Exception {
		Optional<Employee> optional = employeeRepository.findById(id);
		if (!optional.isPresent() || optional.get() == null) {
			return null;
		} else {
			return mapDto(optional.get());
		}
	}

	//Save employee operation.
	public EmployeeDto saveEmployee(Employee employee) throws Exception {
		return mapDto(employeeRepository.save(employee));

	}

	//Delete employee operation.
	public void deleteEmployee(String id) throws Exception {
		employeeRepository.deleteById(id);
	}

	private EmployeeDto mapDto(Employee employee) throws Exception {
		if (employee != null) {
			EmployeeDto dto = new EmployeeDto();
			BeanUtils.copyProperties(employee, dto);
			return dto;
		} else {
			return null;
		}
	}

}
