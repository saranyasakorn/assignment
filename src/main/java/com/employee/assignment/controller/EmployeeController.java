package com.employee.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.employee.assignment.dto.EmployeeDto;
import com.employee.assignment.entity.Employee;
import com.employee.assignment.service.EmployeeService;

@RestController
//@RequestMapping("employee")
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;

	// Get all employee.
	@GetMapping(value = "/employees")
	public ResponseEntity<List<EmployeeDto>> findAll() throws Exception {

		
		List<EmployeeDto> dtoList = employeeService.findAllEmployee();
		if (dtoList.isEmpty() || dtoList == null) {
			return new ResponseEntity<List<EmployeeDto>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<EmployeeDto>>(dtoList,HttpStatus.OK);
		}
	}

	// Get one employee by ID.
	@GetMapping(value = "/employee/{id}")
	public ResponseEntity<EmployeeDto> findById(@PathVariable String id) throws Exception {
		EmployeeDto dto = employeeService.findEmployeeById(id);
		if (dto != null) {
			return new ResponseEntity<EmployeeDto>(dto,HttpStatus.OK);
		} else {
			return new ResponseEntity<EmployeeDto>(HttpStatus.NOT_FOUND);
		}
	}

	// Save a new employee.
	@RequestMapping(value = "/employee", method = { RequestMethod.POST })
	public ResponseEntity<EmployeeDto> save(@RequestBody Employee obj) throws Exception {
		EmployeeDto dto = employeeService.saveEmployee(obj);
		if (dto != null) {
			return new ResponseEntity<EmployeeDto>(dto,HttpStatus.CREATED);
		} else {
			return new ResponseEntity<EmployeeDto>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	// Modify employee by id.
	@RequestMapping(value = "/employee", method = { RequestMethod.PUT })
	public ResponseEntity<EmployeeDto> update(@RequestBody Employee obj) throws Exception {
		if (obj.getId() == null) {
			return new ResponseEntity<EmployeeDto>(HttpStatus.PRECONDITION_REQUIRED);
		} else {
			EmployeeDto dto = employeeService.saveEmployee(obj);
			if (dto != null) {
				return new ResponseEntity<EmployeeDto>(dto,HttpStatus.OK);
			} else {
				return new ResponseEntity<EmployeeDto>(HttpStatus.NOT_FOUND);
			}
		}
	}

	// Delete employee by ID.
	@DeleteMapping(value = "/employee/{id}")
	public ResponseEntity<Object> delete(@PathVariable String id) throws Exception {
		employeeService.deleteEmployee(id);
		return new ResponseEntity<Object>(HttpStatus.OK);

	}
}
