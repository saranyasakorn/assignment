package com.employee.assignment.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.employee.assignment.entity.Employee;

@Repository
public interface  EmployeeRepository extends CrudRepository<Employee, String> {

}
