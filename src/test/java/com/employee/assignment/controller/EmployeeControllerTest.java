package com.employee.assignment.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.employee.assignment.AbstractTest;
import com.employee.assignment.entity.Employee;

public class EmployeeControllerTest extends AbstractTest {
	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void getEmployeesList() throws Exception {
		String uri = "/employees";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Employee[] employeelist = super.mapFromJson(content, Employee[].class);
		assertTrue(employeelist.length > 0);
	}
	
	@Test
	public void getEmployee() throws Exception {
		String uri = "/employee/11";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Employee employee = super.mapFromJson(content, Employee.class);
		assertTrue(employee != null);
	}

	@Test
	public void createEmployee() throws Exception {
		String uri = "/employee";
		Employee employee = new Employee();
		employee.setId("11");
		employee.setName("Saranya Sakorn");
		String inputJson = super.mapToJson(employee);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(201, status);
	}

	@Test
	public void updateEmployee() throws Exception {
		String uri = "/employee";
		Employee employee = new Employee();
		employee.setId("11");
		employee.setName("Saranya Sakornka");
		String inputJson = super.mapToJson(employee);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	@Test
	public void deleteEmployee() throws Exception {
		String uri = "/employee/11";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
}
